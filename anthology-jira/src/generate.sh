#!/usr/bin/sh
# Generate rust structs for json schemas.
# Use go-jira/jira fetch-schemas to get the json schemas.
# $ ./generate.sh $(realpath *.json) 
# Then build the mod.rs file using the following.
# basename -s .rs *.rs | sed 's/^/pub mod /' | sed 's/$/;/' > mod.rs

main() {
	for f in "$@"
	do
		local struct_name="$(basename $f .json)"
		schemafy $f --root "$struct_name" -o "src/rest/model/${struct_name}.rs"
	done
}

main "$@"
