/*
On pause until a bug is fixed in graphql-parser:
https://github.com/graphql-rust/graphql-parser/issues/64
*/
/*
use anthology_model::Issue;
use graphql_client::{GraphQLQuery, Response};
use reqwest::header::AUTHORIZATION;
#[derive(GraphQLQuery)]
#[graphql(
    schema_path = "graphql/jira.graphql",
    query_path = "graphql/jira_queries.graphql",
    response_derives = "Debug"
)]
pub struct GetCloudId;

#[derive(GraphQLQuery)]
#[graphql(
    schema_path = "graphql/jira.graphql",
    query_path = "graphql/jira_queries.graphql",
    response_derives = "Debug"
)]
pub struct ListProjects;

#[derive(GraphQLQuery)]
#[graphql(
    schema_path = "graphql/jira.graphql",
    query_path = "graphql/jira_queries.graphql",
    response_derives = "Debug"
)]
pub struct SoftwareBoardScopeCriticalData;
*/
