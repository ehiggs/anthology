use serde::{Deserialize, Serialize};
#[derive(Clone, PartialEq, Debug, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct IssueRefFieldsIssuetype {
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "avatarId")]
    pub avatar_id: Option<i64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub description: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "iconUrl")]
    pub icon_url: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub id: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,
    pub subtask: bool,
}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct IssueRefFieldsPriority {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub description: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "iconUrl")]
    pub icon_url: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub id: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "statusColor")]
    pub status_color: Option<String>,
}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct IssueRefFieldsStatusStatusCategory {
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "colorName")]
    pub color_name: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub id: Option<i64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub key: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,
}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct IssueRefFieldsStatus {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub description: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "iconUrl")]
    pub icon_url: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub id: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "statusCategory")]
    pub status_category: Option<IssueRefFieldsStatusStatusCategory>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "statusColor")]
    pub status_color: Option<String>,
}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct IssueRefFields {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub issuetype: Option<IssueRefFieldsIssuetype>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub priority: Option<IssueRefFieldsPriority>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub status: Option<IssueRefFieldsStatus>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub summary: Option<String>,
}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(rename = "issue-ref")]
#[serde(deny_unknown_fields)]
pub struct IssueRef {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub fields: Option<IssueRefFields>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub id: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub key: Option<String>,
}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
pub struct UserAvatarUrls {}
#[derive(Clone, PartialEq, Debug, Deserialize, Serialize)]
#[serde(rename = "user")]
#[serde(deny_unknown_fields)]
pub struct User {
    pub active: bool,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "avatarUrls")]
    pub avatar_urls: Option<UserAvatarUrls>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "displayName")]
    pub display_name: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "emailAddress")]
    pub email_address: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub key: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "timeZone")]
    pub time_zone: Option<String>,
}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct LinkIssueRequestCommentItemProperties {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub key: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub value: Option<serde_json::Value>,
}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct LinkIssueRequestCommentVisibility {
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "type")]
    pub type_: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub value: Option<String>,
}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct LinkIssueRequestComment {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub author: Option<User>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub body: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub created: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub id: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub properties: Option<Vec<LinkIssueRequestCommentItemProperties>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "renderedBody")]
    pub rendered_body: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "updateAuthor")]
    pub update_author: Option<User>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub updated: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub visibility: Option<LinkIssueRequestCommentVisibility>,
}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct LinkIssueRequestType {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub id: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub inward: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub outward: Option<String>,
}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct LinkIssueRequest {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub comment: Option<LinkIssueRequestComment>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "inwardIssue")]
    pub inward_issue: Option<IssueRef>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "outwardIssue")]
    pub outward_issue: Option<IssueRef>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "type")]
    pub type_: Option<LinkIssueRequestType>,
}
