use serde::{Deserialize, Serialize};
#[derive(Clone, PartialEq, Debug, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct AttachmentArchiveImplItemEntries {
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "abbreviatedName")]
    pub abbreviated_name: Option<String>,
    #[serde(rename = "entryIndex")]
    pub entry_index: i64,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "mediaType")]
    pub media_type: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,
    pub size: i64,
}
#[derive(Clone, PartialEq, Debug, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct AttachmentArchiveImpl {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub entries: Option<Vec<AttachmentArchiveImplItemEntries>>,
    #[serde(rename = "totalEntryCount")]
    pub total_entry_count: i64,
}
