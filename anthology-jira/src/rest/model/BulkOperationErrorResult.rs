use serde::{Deserialize, Serialize};
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
pub struct BulkOperationErrorResultElementErrorsErrors {}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct BulkOperationErrorResultElementErrors {
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "errorMessages")]
    pub error_messages: Option<Vec<String>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub errors: Option<BulkOperationErrorResultElementErrorsErrors>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub status: Option<i64>,
}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct BulkOperationErrorResult {
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "elementErrors")]
    pub element_errors: Option<BulkOperationErrorResultElementErrors>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "failedElementNumber")]
    pub failed_element_number: Option<i64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub status: Option<i64>,
}
