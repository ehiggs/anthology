use serde::{Deserialize, Serialize};
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct AddField {
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "fieldId")]
    pub field_id: Option<String>,
}
