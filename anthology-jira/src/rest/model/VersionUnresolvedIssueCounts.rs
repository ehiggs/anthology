use serde::{Deserialize, Serialize};
#[derive(Clone, PartialEq, Debug, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct VersionUnresolvedIssueCounts {
    #[serde(rename = "issuesUnresolvedCount")]
    pub issues_unresolved_count: i64,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "self")]
    pub self_: Option<String>,
}
