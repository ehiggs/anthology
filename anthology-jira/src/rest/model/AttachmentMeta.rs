use serde::{Deserialize, Serialize};
#[derive(Clone, PartialEq, Debug, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct AttachmentMeta {
    pub enabled: bool,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "uploadLimit")]
    pub upload_limit: Option<i64>,
}
