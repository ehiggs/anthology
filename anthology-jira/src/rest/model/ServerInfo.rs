use serde::{Deserialize, Serialize};
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct ServerInfoItemHealthChecks {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub description: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub passed: Option<bool>,
}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct ServerInfo {
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "baseUrl")]
    pub base_url: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "buildDate")]
    pub build_date: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "buildNumber")]
    pub build_number: Option<i64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "buildPartnerName")]
    pub build_partner_name: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "deploymentType")]
    pub deployment_type: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "healthChecks")]
    pub health_checks: Option<Vec<ServerInfoItemHealthChecks>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "scmInfo")]
    pub scm_info: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "serverTime")]
    pub server_time: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "serverTitle")]
    pub server_title: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub version: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "versionNumbers")]
    pub version_numbers: Option<Vec<i64>>,
}
