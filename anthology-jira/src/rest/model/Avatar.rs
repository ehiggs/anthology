use serde::{Deserialize, Serialize};
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
pub struct AvatarUrls {}
#[derive(Clone, PartialEq, Debug, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct Avatar {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub id: Option<String>,
    #[serde(rename = "isDeletable")]
    pub is_deletable: bool,
    #[serde(rename = "isSelected")]
    pub is_selected: bool,
    #[serde(rename = "isSystemAvatar")]
    pub is_system_avatar: bool,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub owner: Option<String>,
    pub selected: bool,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub urls: Option<AvatarUrls>,
}
