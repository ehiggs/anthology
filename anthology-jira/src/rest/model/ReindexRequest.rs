use serde::{Deserialize, Serialize};
#[derive(Clone, PartialEq, Debug, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct ReindexRequest {
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "completionTime")]
    pub completion_time: Option<String>,
    pub id: i64,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "requestTime")]
    pub request_time: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "startTime")]
    pub start_time: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub status: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "type")]
    pub type_: Option<String>,
}
