use serde::{Deserialize, Serialize};
#[derive(Clone, PartialEq, Debug, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct ConfigurationTimeTrackingConfiguration {
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "defaultUnit")]
    pub default_unit: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "timeFormat")]
    pub time_format: Option<String>,
    #[serde(rename = "workingDaysPerWeek")]
    pub working_days_per_week: f64,
    #[serde(rename = "workingHoursPerDay")]
    pub working_hours_per_day: f64,
}
#[derive(Clone, PartialEq, Debug, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct Configuration {
    #[serde(rename = "attachmentsEnabled")]
    pub attachments_enabled: bool,
    #[serde(rename = "issueLinkingEnabled")]
    pub issue_linking_enabled: bool,
    #[serde(rename = "subTasksEnabled")]
    pub sub_tasks_enabled: bool,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "timeTrackingConfiguration")]
    pub time_tracking_configuration: Option<ConfigurationTimeTrackingConfiguration>,
    #[serde(rename = "timeTrackingEnabled")]
    pub time_tracking_enabled: bool,
    #[serde(rename = "unassignedIssuesAllowed")]
    pub unassigned_issues_allowed: bool,
    #[serde(rename = "votingEnabled")]
    pub voting_enabled: bool,
    #[serde(rename = "watchingEnabled")]
    pub watching_enabled: bool,
}
