use serde::{Deserialize, Serialize};
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct SimpleListWrapperItemItems {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "self")]
    pub self_: Option<String>,
}
#[derive(Clone, PartialEq, Debug, Deserialize, Serialize)]
#[serde(rename = "simple-list-wrapper")]
#[serde(deny_unknown_fields)]
pub struct SimpleListWrapper {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub items: Option<Vec<SimpleListWrapperItemItems>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "max-results")]
    pub max_results: Option<i64>,
    pub size: i64,
}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
pub struct WorkflowSchemeIssueTypeMappings {}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
pub struct WorkflowSchemeIssueTypes {}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
pub struct WorkflowSchemeLastModifiedUserAvatarUrls {}
#[derive(Clone, PartialEq, Debug, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct WorkflowSchemeLastModifiedUser {
    pub active: bool,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "applicationRoles")]
    pub application_roles: Option<SimpleListWrapper>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "avatarUrls")]
    pub avatar_urls: Option<WorkflowSchemeLastModifiedUserAvatarUrls>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "displayName")]
    pub display_name: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "emailAddress")]
    pub email_address: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub expand: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub groups: Option<SimpleListWrapper>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub key: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub locale: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "self")]
    pub self_: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "timeZone")]
    pub time_zone: Option<String>,
}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
pub struct WorkflowSchemeOriginalIssueTypeMappings {}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct WorkflowScheme {
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "defaultWorkflow")]
    pub default_workflow: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub description: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub draft: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub id: Option<i64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "issueTypeMappings")]
    pub issue_type_mappings: Option<WorkflowSchemeIssueTypeMappings>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "issueTypes")]
    pub issue_types: Option<WorkflowSchemeIssueTypes>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "lastModified")]
    pub last_modified: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "lastModifiedUser")]
    pub last_modified_user: Option<WorkflowSchemeLastModifiedUser>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "originalDefaultWorkflow")]
    pub original_default_workflow: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "originalIssueTypeMappings")]
    pub original_issue_type_mappings: Option<WorkflowSchemeOriginalIssueTypeMappings>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "self")]
    pub self_: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "updateDraftIfNeeded")]
    pub update_draft_if_needed: Option<bool>,
}
