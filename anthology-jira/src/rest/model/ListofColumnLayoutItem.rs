use serde::{Deserialize, Serialize};
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct ListofColumnLayoutItemItemNavigableFieldSorter {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub comparator: Option<::std::collections::BTreeMap<String, serde_json::Value>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "documentConstant")]
    pub document_constant: Option<String>,
}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct ListofColumnLayoutItemItemNavigableField {
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "columnCssClass")]
    pub column_css_class: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "columnHeadingKey")]
    pub column_heading_key: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "defaultSortOrder")]
    pub default_sort_order: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "hiddenFieldId")]
    pub hidden_field_id: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "sortComparatorSource")]
    pub sort_comparator_source: Option<::std::collections::BTreeMap<String, serde_json::Value>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub sorter: Option<ListofColumnLayoutItemItemNavigableFieldSorter>,
}
#[derive(Clone, PartialEq, Debug, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct ListofColumnLayoutItemItem {
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "columnHeadingKey")]
    pub column_heading_key: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub id: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "navigableField")]
    pub navigable_field: Option<ListofColumnLayoutItemItemNavigableField>,
    pub position: i64,
}
pub type ListofColumnLayoutItem = Vec<ListofColumnLayoutItemItem>;
