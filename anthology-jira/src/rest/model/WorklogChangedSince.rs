use serde::{Deserialize, Serialize};
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct WorklogChangedSinceItemValues {
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "updatedTime")]
    pub updated_time: Option<i64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "worklogId")]
    pub worklog_id: Option<i64>,
}
#[derive(Clone, PartialEq, Debug, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct WorklogChangedSince {
    #[serde(rename = "isLastPage")]
    pub is_last_page: bool,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "nextPage")]
    pub next_page: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "self")]
    pub self_: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub since: Option<i64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub until: Option<i64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub values: Option<Vec<WorklogChangedSinceItemValues>>,
}
