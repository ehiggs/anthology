use serde::{Deserialize, Serialize};
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct Default {
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "updateDraftIfNeeded")]
    pub update_draft_if_needed: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub workflow: Option<String>,
}
