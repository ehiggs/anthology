use serde::{Deserialize, Serialize};
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct AutoCompleteResponse {
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "jqlReservedWords")]
    pub jql_reserved_words: Option<Vec<String>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "visibleFieldNames")]
    pub visible_field_names: Option<Vec<String>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "visibleFunctionNames")]
    pub visible_function_names: Option<Vec<String>>,
}
