use serde::{Deserialize, Serialize};
pub type Response = ::std::collections::BTreeMap<String, serde_json::Value>;
