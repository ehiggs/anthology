use serde::{Deserialize, Serialize};
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
pub struct GroupUsersItemItemsAvatarUrls {}
#[derive(Clone, PartialEq, Debug, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct GroupUsersItemItems {
    pub active: bool,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "avatarUrls")]
    pub avatar_urls: Option<GroupUsersItemItemsAvatarUrls>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "displayName")]
    pub display_name: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "emailAddress")]
    pub email_address: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub key: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "self")]
    pub self_: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "timeZone")]
    pub time_zone: Option<String>,
}
#[derive(Clone, PartialEq, Debug, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct GroupUsers {
    #[serde(rename = "end-index")]
    pub end_index: i64,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub items: Option<Vec<GroupUsersItemItems>>,
    #[serde(rename = "max-results")]
    pub max_results: i64,
    pub size: i64,
    #[serde(rename = "start-index")]
    pub start_index: i64,
}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct Group {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub expand: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "self")]
    pub self_: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub users: Option<GroupUsers>,
}
