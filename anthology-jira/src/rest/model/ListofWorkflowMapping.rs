use serde::{Deserialize, Serialize};
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct ListofWorkflowMappingItem {
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "defaultMapping")]
    pub default_mapping: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "issueTypes")]
    pub issue_types: Option<Vec<String>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "updateDraftIfNeeded")]
    pub update_draft_if_needed: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub workflow: Option<String>,
}
pub type ListofWorkflowMapping = Vec<ListofWorkflowMappingItem>;
