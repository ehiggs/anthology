use serde::{Deserialize, Serialize};
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(rename = "group")]
#[serde(deny_unknown_fields)]
pub struct Group {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "self")]
    pub self_: Option<String>,
}
#[derive(Clone, PartialEq, Debug, Deserialize, Serialize)]
#[serde(rename = "simple-list-wrapper")]
#[serde(deny_unknown_fields)]
pub struct SimpleListWrapper {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub items: Option<Vec<Group>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "max-results")]
    pub max_results: Option<i64>,
    pub size: i64,
}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
pub struct UserAvatarUrls {}
#[derive(Clone, PartialEq, Debug, Deserialize, Serialize)]
#[serde(rename = "user")]
#[serde(deny_unknown_fields)]
pub struct User {
    pub active: bool,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "applicationRoles")]
    pub application_roles: Option<SimpleListWrapper>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "avatarUrls")]
    pub avatar_urls: Option<UserAvatarUrls>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "displayName")]
    pub display_name: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "emailAddress")]
    pub email_address: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub expand: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub groups: Option<SimpleListWrapper>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub key: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub locale: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "self")]
    pub self_: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "timeZone")]
    pub time_zone: Option<String>,
}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
pub struct FilterItemSharePermissionsProjectAvatarUrls {}
#[derive(Clone, PartialEq, Debug, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct FilterItemSharePermissionsProjectItemComponents {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub assignee: Option<User>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "assigneeType")]
    pub assignee_type: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub description: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub id: Option<String>,
    #[serde(rename = "isAssigneeTypeValid")]
    pub is_assignee_type_valid: bool,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub lead: Option<User>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "leadUserName")]
    pub lead_user_name: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub project: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "projectId")]
    pub project_id: Option<i64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "realAssignee")]
    pub real_assignee: Option<User>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "realAssigneeType")]
    pub real_assignee_type: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "self")]
    pub self_: Option<String>,
}
#[derive(Clone, PartialEq, Debug, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct FilterItemSharePermissionsProjectItemIssueTypes {
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "avatarId")]
    pub avatar_id: Option<i64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub description: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "iconUrl")]
    pub icon_url: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub id: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "self")]
    pub self_: Option<String>,
    pub subtask: bool,
}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct FilterItemSharePermissionsProjectProjectCategory {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub description: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub id: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "self")]
    pub self_: Option<String>,
}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
pub struct FilterItemSharePermissionsProjectRoles {}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct FilterItemSharePermissionsProjectItemVersionsItemOperations {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub href: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "iconClass")]
    pub icon_class: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub id: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub label: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "styleClass")]
    pub style_class: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub title: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub weight: Option<i64>,
}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct FilterItemSharePermissionsProjectItemVersionsItemRemotelinks {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub link: Option<serde_json::Value>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "self")]
    pub self_: Option<String>,
}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct FilterItemSharePermissionsProjectItemVersions {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub archived: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub description: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub expand: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub id: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "moveUnfixedIssuesTo")]
    pub move_unfixed_issues_to: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub operations: Option<Vec<FilterItemSharePermissionsProjectItemVersionsItemOperations>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub overdue: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub project: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "projectId")]
    pub project_id: Option<i64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub released: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub remotelinks: Option<Vec<FilterItemSharePermissionsProjectItemVersionsItemRemotelinks>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "self")]
    pub self_: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "userReleaseDate")]
    pub user_release_date: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "userStartDate")]
    pub user_start_date: Option<String>,
}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct FilterItemSharePermissionsProject {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub archived: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "assigneeType")]
    pub assignee_type: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "avatarUrls")]
    pub avatar_urls: Option<FilterItemSharePermissionsProjectAvatarUrls>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub components: Option<Vec<FilterItemSharePermissionsProjectItemComponents>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub description: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub email: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub expand: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub id: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "issueTypes")]
    pub issue_types: Option<Vec<FilterItemSharePermissionsProjectItemIssueTypes>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub key: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub lead: Option<User>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "projectCategory")]
    pub project_category: Option<FilterItemSharePermissionsProjectProjectCategory>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "projectKeys")]
    pub project_keys: Option<Vec<String>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "projectTypeKey")]
    pub project_type_key: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub roles: Option<FilterItemSharePermissionsProjectRoles>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "self")]
    pub self_: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub url: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub versions: Option<Vec<FilterItemSharePermissionsProjectItemVersions>>,
}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct FilterItemSharePermissionsRoleItemActors {
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "avatarUrl")]
    pub avatar_url: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "displayName")]
    pub display_name: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub id: Option<i64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "type")]
    pub type_: Option<String>,
}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct FilterItemSharePermissionsRole {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub actors: Option<Vec<FilterItemSharePermissionsRoleItemActors>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub description: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub id: Option<i64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "self")]
    pub self_: Option<String>,
}
#[derive(Clone, PartialEq, Debug, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct FilterItemSharePermissions {
    pub edit: bool,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub group: Option<Group>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub id: Option<i64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub project: Option<FilterItemSharePermissionsProject>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub role: Option<FilterItemSharePermissionsRole>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "type")]
    pub type_: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub user: Option<User>,
    pub view: bool,
}
#[derive(Clone, PartialEq, Debug, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct FilterSharedUsers {
    #[serde(rename = "end-index")]
    pub end_index: i64,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub items: Option<Vec<User>>,
    #[serde(rename = "max-results")]
    pub max_results: i64,
    pub size: i64,
    #[serde(rename = "start-index")]
    pub start_index: i64,
}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct FilterSubscriptionsItemItems {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub group: Option<Group>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub id: Option<i64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub user: Option<User>,
}
#[derive(Clone, PartialEq, Debug, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct FilterSubscriptions {
    #[serde(rename = "end-index")]
    pub end_index: i64,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub items: Option<Vec<FilterSubscriptionsItemItems>>,
    #[serde(rename = "max-results")]
    pub max_results: i64,
    pub size: i64,
    #[serde(rename = "start-index")]
    pub start_index: i64,
}
#[derive(Clone, PartialEq, Debug, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct Filter {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub description: Option<String>,
    pub editable: bool,
    pub favourite: bool,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub id: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub jql: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub owner: Option<User>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "searchUrl")]
    pub search_url: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "self")]
    pub self_: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "sharePermissions")]
    pub share_permissions: Option<Vec<FilterItemSharePermissions>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "sharedUsers")]
    pub shared_users: Option<FilterSharedUsers>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub subscriptions: Option<FilterSubscriptions>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "viewUrl")]
    pub view_url: Option<String>,
}
