use serde::{Deserialize, Serialize};
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct ListofApplicationRoleItem {
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "defaultGroups")]
    pub default_groups: Option<Vec<String>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub defined: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub groups: Option<Vec<String>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "hasUnlimitedSeats")]
    pub has_unlimited_seats: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub key: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "numberOfSeats")]
    pub number_of_seats: Option<i64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub platform: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "remainingSeats")]
    pub remaining_seats: Option<i64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "selectedByDefault")]
    pub selected_by_default: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "userCount")]
    pub user_count: Option<i64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "userCountDescription")]
    pub user_count_description: Option<String>,
}
pub type ListofApplicationRole = Vec<ListofApplicationRoleItem>;
