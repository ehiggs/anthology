use serde::{Deserialize, Serialize};
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
pub struct ProjectRoleActorsUpdateCategorisedActors {}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct ProjectRoleActorsUpdate {
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "categorisedActors")]
    pub categorised_actors: Option<ProjectRoleActorsUpdateCategorisedActors>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub id: Option<i64>,
}
