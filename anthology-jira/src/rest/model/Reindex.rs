use serde::{Deserialize, Serialize};
#[derive(Clone, PartialEq, Debug, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct Reindex {
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "currentProgress")]
    pub current_progress: Option<i64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "currentSubTask")]
    pub current_sub_task: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "finishTime")]
    pub finish_time: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "progressUrl")]
    pub progress_url: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "startTime")]
    pub start_time: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "submittedTime")]
    pub submitted_time: Option<String>,
    pub success: bool,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "type")]
    pub type_: Option<String>,
}
