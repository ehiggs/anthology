use serde::{Deserialize, Serialize};
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct CurrentUserLoginInfo {
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "failedLoginCount")]
    pub failed_login_count: Option<i64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "lastFailedLoginTime")]
    pub last_failed_login_time: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "loginCount")]
    pub login_count: Option<i64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "previousLoginTime")]
    pub previous_login_time: Option<String>,
}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct CurrentUser {
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "loginInfo")]
    pub login_info: Option<CurrentUserLoginInfo>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "self")]
    pub self_: Option<String>,
}
