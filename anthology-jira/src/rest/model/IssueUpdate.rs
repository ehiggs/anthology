use serde::{Deserialize, Serialize};
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(rename = "history-metadata-participant")]
#[serde(deny_unknown_fields)]
pub struct HistoryMetadataParticipant {
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "avatarUrl")]
    pub avatar_url: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "displayName")]
    pub display_name: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "displayNameKey")]
    pub display_name_key: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub id: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "type")]
    pub type_: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub url: Option<String>,
}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
pub struct IssueUpdateFields {}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
pub struct IssueUpdateHistoryMetadataExtraData {}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct IssueUpdateHistoryMetadata {
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "activityDescription")]
    pub activity_description: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "activityDescriptionKey")]
    pub activity_description_key: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub actor: Option<HistoryMetadataParticipant>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub cause: Option<HistoryMetadataParticipant>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub description: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "descriptionKey")]
    pub description_key: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "emailDescription")]
    pub email_description: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "emailDescriptionKey")]
    pub email_description_key: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "extraData")]
    pub extra_data: Option<IssueUpdateHistoryMetadataExtraData>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub generator: Option<HistoryMetadataParticipant>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "type")]
    pub type_: Option<String>,
}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct IssueUpdateItemProperties {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub key: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub value: Option<serde_json::Value>,
}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
pub struct IssueUpdateTransitionFields {}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct IssueUpdateTransitionToStatusCategory {
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "colorName")]
    pub color_name: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub id: Option<i64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub key: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,
}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct IssueUpdateTransitionTo {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub description: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "iconUrl")]
    pub icon_url: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub id: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "statusCategory")]
    pub status_category: Option<IssueUpdateTransitionToStatusCategory>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "statusColor")]
    pub status_color: Option<String>,
}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct IssueUpdateTransition {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub fields: Option<IssueUpdateTransitionFields>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub id: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub to: Option<IssueUpdateTransitionTo>,
}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
pub struct IssueUpdateUpdate {}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct IssueUpdate {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub fields: Option<IssueUpdateFields>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "historyMetadata")]
    pub history_metadata: Option<IssueUpdateHistoryMetadata>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub properties: Option<Vec<IssueUpdateItemProperties>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub transition: Option<IssueUpdateTransition>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub update: Option<IssueUpdateUpdate>,
}
