use serde::{Deserialize, Serialize};
pub type DeleteAndReplaceVersion = ::std::collections::BTreeMap<String, serde_json::Value>;
