use serde::{Deserialize, Serialize};
#[derive(Clone, PartialEq, Debug, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct VersionIssueCountsItemCustomFieldUsage {
    #[serde(rename = "customFieldId")]
    pub custom_field_id: i64,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "fieldName")]
    pub field_name: Option<String>,
    #[serde(rename = "issueCountWithVersionInCustomField")]
    pub issue_count_with_version_in_custom_field: i64,
}
#[derive(Clone, PartialEq, Debug, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct VersionIssueCounts {
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "customFieldUsage")]
    pub custom_field_usage: Option<Vec<VersionIssueCountsItemCustomFieldUsage>>,
    #[serde(rename = "issueCountWithCustomFieldsShowingVersion")]
    pub issue_count_with_custom_fields_showing_version: i64,
    #[serde(rename = "issuesAffectedCount")]
    pub issues_affected_count: i64,
    #[serde(rename = "issuesFixedCount")]
    pub issues_fixed_count: i64,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "self")]
    pub self_: Option<String>,
}
