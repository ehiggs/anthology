use serde::{Deserialize, Serialize};
#[derive(Clone, PartialEq, Debug, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct PageofCustomFieldItemValues {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub description: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub id: Option<String>,
    #[serde(rename = "isLocked")]
    pub is_locked: bool,
    #[serde(rename = "isManaged")]
    pub is_managed: bool,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "numericId")]
    pub numeric_id: Option<i64>,
    #[serde(rename = "projectsCount")]
    pub projects_count: i64,
    #[serde(rename = "screensCount")]
    pub screens_count: i64,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "searcherKey")]
    pub searcher_key: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "self")]
    pub self_: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "type")]
    pub type_: Option<String>,
}
#[derive(Clone, PartialEq, Debug, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct PageofCustomField {
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "isLast")]
    pub is_last: Option<bool>,
    #[serde(rename = "maxResults")]
    pub max_results: i64,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "nextPage")]
    pub next_page: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "self")]
    pub self_: Option<String>,
    #[serde(rename = "startAt")]
    pub start_at: i64,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub total: Option<i64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub values: Option<Vec<PageofCustomFieldItemValues>>,
}
