use serde::{Deserialize, Serialize};
#[derive(Clone, PartialEq, Debug, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct IssueSubTaskMovePosition {
    pub current: i64,
    pub original: i64,
}
