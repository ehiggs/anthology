use serde::{Deserialize, Serialize};
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
pub struct ErrorCollectionErrors {}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct ErrorCollection {
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "errorMessages")]
    pub error_messages: Option<Vec<String>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub errors: Option<ErrorCollectionErrors>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub status: Option<i64>,
}
