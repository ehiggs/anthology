use serde::{Deserialize, Serialize};
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct UpdateUserToGroup {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,
}
