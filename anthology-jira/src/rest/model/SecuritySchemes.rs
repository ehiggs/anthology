use serde::{Deserialize, Serialize};
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct SecuritySchemesItemIssueSecuritySchemesItemLevels {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub description: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub id: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "self")]
    pub self_: Option<String>,
}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct SecuritySchemesItemIssueSecuritySchemes {
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "defaultSecurityLevelId")]
    pub default_security_level_id: Option<i64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub description: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub id: Option<i64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub levels: Option<Vec<SecuritySchemesItemIssueSecuritySchemesItemLevels>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "self")]
    pub self_: Option<String>,
}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct SecuritySchemes {
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "issueSecuritySchemes")]
    pub issue_security_schemes: Option<Vec<SecuritySchemesItemIssueSecuritySchemes>>,
}
