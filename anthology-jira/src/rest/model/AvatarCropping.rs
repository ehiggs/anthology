use serde::{Deserialize, Serialize};
#[derive(Clone, PartialEq, Debug, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct AvatarCropping {
    #[serde(rename = "cropperOffsetX")]
    pub cropper_offset_x: i64,
    #[serde(rename = "cropperOffsetY")]
    pub cropper_offset_y: i64,
    #[serde(rename = "cropperWidth")]
    pub cropper_width: i64,
    #[serde(rename = "needsCropping")]
    pub needs_cropping: bool,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub url: Option<String>,
}
