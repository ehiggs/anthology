use serde::{Deserialize, Serialize};
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(rename = "index-replication-queue-entry")]
#[serde(deny_unknown_fields)]
pub struct IndexReplicationQueueEntry {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub id: Option<i64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "replicationTime")]
    pub replication_time: Option<::std::collections::BTreeMap<String, serde_json::Value>>,
}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct IndexSummaryIssueIndex {
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "countInArchive")]
    pub count_in_archive: Option<i64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "countInDatabase")]
    pub count_in_database: Option<i64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "countInIndex")]
    pub count_in_index: Option<i64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "indexReadable")]
    pub index_readable: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "lastUpdatedInDatabase")]
    pub last_updated_in_database: Option<::std::collections::BTreeMap<String, serde_json::Value>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "lastUpdatedInIndex")]
    pub last_updated_in_index: Option<::std::collections::BTreeMap<String, serde_json::Value>>,
}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
pub struct IndexSummaryReplicationQueues {}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct IndexSummary {
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "issueIndex")]
    pub issue_index: Option<IndexSummaryIssueIndex>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "nodeId")]
    pub node_id: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "replicationQueues")]
    pub replication_queues: Option<IndexSummaryReplicationQueues>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "reportTime")]
    pub report_time: Option<::std::collections::BTreeMap<String, serde_json::Value>>,
}
