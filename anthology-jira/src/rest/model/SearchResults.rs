use serde::{Deserialize, Serialize};
#[derive(Clone, PartialEq, Debug, Deserialize, Serialize)]
#[serde(rename = "field-meta")]
#[serde(deny_unknown_fields)]
pub struct FieldMeta {
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "allowedValues")]
    pub allowed_values: Option<Vec<serde_json::Value>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "autoCompleteUrl")]
    pub auto_complete_url: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "hasDefaultValue")]
    pub has_default_value: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub operations: Option<Vec<String>>,
    pub required: bool,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub schema: Option<JsonType>,
}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(rename = "history-metadata-participant")]
#[serde(deny_unknown_fields)]
pub struct HistoryMetadataParticipant {
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "avatarUrl")]
    pub avatar_url: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "displayName")]
    pub display_name: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "displayNameKey")]
    pub display_name_key: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub id: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "type")]
    pub type_: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub url: Option<String>,
}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(rename = "json-type")]
#[serde(deny_unknown_fields)]
pub struct JsonType {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub custom: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "customId")]
    pub custom_id: Option<i64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub items: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub system: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "type")]
    pub type_: Option<String>,
}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(rename = "link-group")]
#[serde(deny_unknown_fields)]
pub struct LinkGroup {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub groups: Option<Vec<LinkGroup>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub header: Option<SimpleLink>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub id: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub links: Option<Vec<SimpleLink>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "styleClass")]
    pub style_class: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub weight: Option<i64>,
}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(rename = "simple-link")]
#[serde(deny_unknown_fields)]
pub struct SimpleLink {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub href: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "iconClass")]
    pub icon_class: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub id: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub label: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "styleClass")]
    pub style_class: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub title: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub weight: Option<i64>,
}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
pub struct SearchResultsItemIssuesChangelogItemHistoriesAuthorAvatarUrls {}
#[derive(Clone, PartialEq, Debug, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct SearchResultsItemIssuesChangelogItemHistoriesAuthor {
    pub active: bool,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "avatarUrls")]
    pub avatar_urls: Option<SearchResultsItemIssuesChangelogItemHistoriesAuthorAvatarUrls>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "displayName")]
    pub display_name: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "emailAddress")]
    pub email_address: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub key: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "self")]
    pub self_: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "timeZone")]
    pub time_zone: Option<String>,
}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
pub struct SearchResultsItemIssuesChangelogItemHistoriesHistoryMetadataExtraData {}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct SearchResultsItemIssuesChangelogItemHistoriesHistoryMetadata {
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "activityDescription")]
    pub activity_description: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "activityDescriptionKey")]
    pub activity_description_key: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub actor: Option<HistoryMetadataParticipant>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub cause: Option<HistoryMetadataParticipant>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub description: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "descriptionKey")]
    pub description_key: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "emailDescription")]
    pub email_description: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "emailDescriptionKey")]
    pub email_description_key: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "extraData")]
    pub extra_data: Option<SearchResultsItemIssuesChangelogItemHistoriesHistoryMetadataExtraData>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub generator: Option<HistoryMetadataParticipant>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "type")]
    pub type_: Option<String>,
}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct SearchResultsItemIssuesChangelogItemHistoriesItemItems {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub field: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub fieldtype: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub from: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "fromString")]
    pub from_string: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub to: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "toString")]
    pub to_string: Option<String>,
}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct SearchResultsItemIssuesChangelogItemHistories {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub author: Option<SearchResultsItemIssuesChangelogItemHistoriesAuthor>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub created: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "historyMetadata")]
    pub history_metadata: Option<SearchResultsItemIssuesChangelogItemHistoriesHistoryMetadata>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub id: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub items: Option<Vec<SearchResultsItemIssuesChangelogItemHistoriesItemItems>>,
}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct SearchResultsItemIssuesChangelog {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub histories: Option<Vec<SearchResultsItemIssuesChangelogItemHistories>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "maxResults")]
    pub max_results: Option<i64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "startAt")]
    pub start_at: Option<i64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub total: Option<i64>,
}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
pub struct SearchResultsItemIssuesEditmetaFields {}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct SearchResultsItemIssuesEditmeta {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub fields: Option<SearchResultsItemIssuesEditmetaFields>,
}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
pub struct SearchResultsItemIssuesFields {}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
pub struct SearchResultsItemIssuesNames {}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct SearchResultsItemIssuesOperations {
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "linkGroups")]
    pub link_groups: Option<Vec<LinkGroup>>,
}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
pub struct SearchResultsItemIssuesPropertiesProperties {}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct SearchResultsItemIssuesProperties {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub properties: Option<SearchResultsItemIssuesPropertiesProperties>,
}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
pub struct SearchResultsItemIssuesRenderedFields {}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
pub struct SearchResultsItemIssuesSchema {}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
pub struct SearchResultsItemIssuesItemTransitionsFields {}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct SearchResultsItemIssuesItemTransitionsToStatusCategory {
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "colorName")]
    pub color_name: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub id: Option<i64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub key: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "self")]
    pub self_: Option<String>,
}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct SearchResultsItemIssuesItemTransitionsTo {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub description: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "iconUrl")]
    pub icon_url: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub id: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "self")]
    pub self_: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "statusCategory")]
    pub status_category: Option<SearchResultsItemIssuesItemTransitionsToStatusCategory>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "statusColor")]
    pub status_color: Option<String>,
}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct SearchResultsItemIssuesItemTransitions {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub expand: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub fields: Option<SearchResultsItemIssuesItemTransitionsFields>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub id: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub to: Option<SearchResultsItemIssuesItemTransitionsTo>,
}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
pub struct SearchResultsItemIssuesVersionedRepresentations {}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct SearchResultsItemIssues {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub changelog: Option<SearchResultsItemIssuesChangelog>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub editmeta: Option<SearchResultsItemIssuesEditmeta>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub expand: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub fields: Option<SearchResultsItemIssuesFields>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "fieldsToInclude")]
    pub fields_to_include: Option<::std::collections::BTreeMap<String, serde_json::Value>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub id: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub key: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub names: Option<SearchResultsItemIssuesNames>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub operations: Option<SearchResultsItemIssuesOperations>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub properties: Option<SearchResultsItemIssuesProperties>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "renderedFields")]
    pub rendered_fields: Option<SearchResultsItemIssuesRenderedFields>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub schema: Option<SearchResultsItemIssuesSchema>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "self")]
    pub self_: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub transitions: Option<Vec<SearchResultsItemIssuesItemTransitions>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "versionedRepresentations")]
    pub versioned_representations: Option<SearchResultsItemIssuesVersionedRepresentations>,
}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
pub struct SearchResultsNames {}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
pub struct SearchResultsSchema {}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct SearchResults {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub expand: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub issues: Option<Vec<SearchResultsItemIssues>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "maxResults")]
    pub max_results: Option<i64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub names: Option<SearchResultsNames>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub schema: Option<SearchResultsSchema>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "startAt")]
    pub start_at: Option<i64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub total: Option<i64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "warningMessages")]
    pub warning_messages: Option<Vec<String>>,
}
