use serde::{Deserialize, Serialize};
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct PrioritySchemeUpdate {
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "defaultOptionId")]
    pub default_option_id: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub description: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub id: Option<i64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "optionIds")]
    pub option_ids: Option<Vec<String>>,
}
