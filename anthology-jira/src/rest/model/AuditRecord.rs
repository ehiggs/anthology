use serde::{Deserialize, Serialize};
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(rename = "associated-item")]
#[serde(deny_unknown_fields)]
pub struct AssociatedItem {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub id: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "parentId")]
    pub parent_id: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "parentName")]
    pub parent_name: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "typeName")]
    pub type_name: Option<String>,
}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct AuditRecordItemChangedValues {
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "changedFrom")]
    pub changed_from: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "changedTo")]
    pub changed_to: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "fieldName")]
    pub field_name: Option<String>,
}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct AuditRecord {
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "associatedItems")]
    pub associated_items: Option<Vec<AssociatedItem>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "authorKey")]
    pub author_key: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub category: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "changedValues")]
    pub changed_values: Option<Vec<AuditRecordItemChangedValues>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub created: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub description: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "eventSource")]
    pub event_source: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub id: Option<i64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "objectItem")]
    pub object_item: Option<AssociatedItem>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "remoteAddress")]
    pub remote_address: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub summary: Option<String>,
}
