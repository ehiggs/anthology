use serde::{Deserialize, Serialize};
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct Id {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub id: Option<i64>,
}
