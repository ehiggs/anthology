use serde::{Deserialize, Serialize};
#[derive(Clone, PartialEq, Debug, Deserialize, Serialize)]
#[serde(rename = "field-meta")]
#[serde(deny_unknown_fields)]
pub struct FieldMeta {
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "allowedValues")]
    pub allowed_values: Option<Vec<serde_json::Value>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "autoCompleteUrl")]
    pub auto_complete_url: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "hasDefaultValue")]
    pub has_default_value: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub operations: Option<Vec<String>>,
    pub required: bool,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub schema: Option<JsonType>,
}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(rename = "history-metadata-participant")]
#[serde(deny_unknown_fields)]
pub struct HistoryMetadataParticipant {
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "avatarUrl")]
    pub avatar_url: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "displayName")]
    pub display_name: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "displayNameKey")]
    pub display_name_key: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub id: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "type")]
    pub type_: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub url: Option<String>,
}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(rename = "json-type")]
#[serde(deny_unknown_fields)]
pub struct JsonType {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub custom: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "customId")]
    pub custom_id: Option<i64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub items: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub system: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "type")]
    pub type_: Option<String>,
}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(rename = "link-group")]
#[serde(deny_unknown_fields)]
pub struct LinkGroup {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub groups: Option<Vec<LinkGroup>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub header: Option<SimpleLink>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub id: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub links: Option<Vec<SimpleLink>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "styleClass")]
    pub style_class: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub weight: Option<i64>,
}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(rename = "simple-link")]
#[serde(deny_unknown_fields)]
pub struct SimpleLink {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub href: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "iconClass")]
    pub icon_class: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub id: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub label: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "styleClass")]
    pub style_class: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub title: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub weight: Option<i64>,
}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
pub struct IssueChangelogItemHistoriesAuthorAvatarUrls {}
#[derive(Clone, PartialEq, Debug, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct IssueChangelogItemHistoriesAuthor {
    pub active: bool,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "avatarUrls")]
    pub avatar_urls: Option<IssueChangelogItemHistoriesAuthorAvatarUrls>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "displayName")]
    pub display_name: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "emailAddress")]
    pub email_address: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub key: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "self")]
    pub self_: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "timeZone")]
    pub time_zone: Option<String>,
}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
pub struct IssueChangelogItemHistoriesHistoryMetadataExtraData {}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct IssueChangelogItemHistoriesHistoryMetadata {
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "activityDescription")]
    pub activity_description: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "activityDescriptionKey")]
    pub activity_description_key: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub actor: Option<HistoryMetadataParticipant>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub cause: Option<HistoryMetadataParticipant>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub description: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "descriptionKey")]
    pub description_key: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "emailDescription")]
    pub email_description: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "emailDescriptionKey")]
    pub email_description_key: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "extraData")]
    pub extra_data: Option<IssueChangelogItemHistoriesHistoryMetadataExtraData>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub generator: Option<HistoryMetadataParticipant>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "type")]
    pub type_: Option<String>,
}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct IssueChangelogItemHistoriesItemItems {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub field: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub fieldtype: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub from: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "fromString")]
    pub from_string: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub to: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "toString")]
    pub to_string: Option<String>,
}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct IssueChangelogItemHistories {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub author: Option<IssueChangelogItemHistoriesAuthor>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub created: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "historyMetadata")]
    pub history_metadata: Option<IssueChangelogItemHistoriesHistoryMetadata>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub id: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub items: Option<Vec<IssueChangelogItemHistoriesItemItems>>,
}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct IssueChangelog {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub histories: Option<Vec<IssueChangelogItemHistories>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "maxResults")]
    pub max_results: Option<i64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "startAt")]
    pub start_at: Option<i64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub total: Option<i64>,
}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
pub struct IssueEditmetaFields {}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct IssueEditmeta {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub fields: Option<IssueEditmetaFields>,
}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
pub struct IssueFields {}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
pub struct IssueNames {}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct IssueOperations {
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "linkGroups")]
    pub link_groups: Option<Vec<LinkGroup>>,
}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
pub struct IssuePropertiesProperties {}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct IssueProperties {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub properties: Option<IssuePropertiesProperties>,
}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
pub struct IssueRenderedFields {}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
pub struct IssueSchema {}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
pub struct IssueItemTransitionsFields {}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct IssueItemTransitionsToStatusCategory {
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "colorName")]
    pub color_name: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub id: Option<i64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub key: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "self")]
    pub self_: Option<String>,
}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct IssueItemTransitionsTo {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub description: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "iconUrl")]
    pub icon_url: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub id: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "self")]
    pub self_: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "statusCategory")]
    pub status_category: Option<IssueItemTransitionsToStatusCategory>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "statusColor")]
    pub status_color: Option<String>,
}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct IssueItemTransitions {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub expand: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub fields: Option<IssueItemTransitionsFields>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub id: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub to: Option<IssueItemTransitionsTo>,
}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
pub struct IssueVersionedRepresentations {}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct Issue {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub changelog: Option<IssueChangelog>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub editmeta: Option<IssueEditmeta>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub expand: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub fields: Option<IssueFields>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "fieldsToInclude")]
    pub fields_to_include: Option<::std::collections::BTreeMap<String, serde_json::Value>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub id: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub key: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub names: Option<IssueNames>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub operations: Option<IssueOperations>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub properties: Option<IssueProperties>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "renderedFields")]
    pub rendered_fields: Option<IssueRenderedFields>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub schema: Option<IssueSchema>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "self")]
    pub self_: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub transitions: Option<Vec<IssueItemTransitions>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "versionedRepresentations")]
    pub versioned_representations: Option<IssueVersionedRepresentations>,
}
