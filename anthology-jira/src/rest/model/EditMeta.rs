use serde::{Deserialize, Serialize};
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
pub struct EditMetaFields {}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct EditMeta {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub fields: Option<EditMetaFields>,
}
