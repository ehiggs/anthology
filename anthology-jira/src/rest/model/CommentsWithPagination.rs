use serde::{Deserialize, Serialize};
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
pub struct UserAvatarUrls {}
#[derive(Clone, PartialEq, Debug, Deserialize, Serialize)]
#[serde(rename = "user")]
#[serde(deny_unknown_fields)]
pub struct User {
    pub active: bool,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "avatarUrls")]
    pub avatar_urls: Option<UserAvatarUrls>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "displayName")]
    pub display_name: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "emailAddress")]
    pub email_address: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub key: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "self")]
    pub self_: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "timeZone")]
    pub time_zone: Option<String>,
}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct CommentsWithPaginationItemCommentsItemProperties {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub key: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub value: Option<serde_json::Value>,
}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct CommentsWithPaginationItemCommentsVisibility {
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "type")]
    pub type_: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub value: Option<String>,
}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct CommentsWithPaginationItemComments {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub author: Option<User>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub body: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub created: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub id: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub properties: Option<Vec<CommentsWithPaginationItemCommentsItemProperties>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "renderedBody")]
    pub rendered_body: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "self")]
    pub self_: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "updateAuthor")]
    pub update_author: Option<User>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub updated: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub visibility: Option<CommentsWithPaginationItemCommentsVisibility>,
}
#[derive(Clone, PartialEq, Debug, Default, Deserialize, Serialize)]
#[serde(deny_unknown_fields)]
pub struct CommentsWithPagination {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub comments: Option<Vec<CommentsWithPaginationItemComments>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "maxResults")]
    pub max_results: Option<i64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    #[serde(rename = "startAt")]
    pub start_at: Option<i64>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub total: Option<i64>,
}
