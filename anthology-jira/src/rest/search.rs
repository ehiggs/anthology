use crate::rest::model::SearchRequest::SearchRequest;
use crate::rest::model::SearchResults::{SearchResults, SearchResultsItemIssues};
use crate::rest::Client;
use anthology_model::{AnthologyError, IssueListEntry};
use reqwest;
use reqwest::header::AUTHORIZATION;

struct SearchOptions {
    pub assignee: Option<String>,
    pub query: Option<String>,
    pub query_fields: Option<String>,
    pub project: Option<String>,
    pub component: Option<String>,
    pub reporter: Option<String>,
    pub issue_type: Option<String>,
    pub watcher: Option<String>,
    pub status: Option<String>,
    pub sort: Option<String>,
    pub max_results: usize,
}

impl Into<SearchRequest> for SearchOptions {
    fn into(self) -> SearchRequest {
        let mut fields = vec!["summary".to_string()];
        if let Some(f) = self.query_fields {
            let mut split_fields = f.split(",").map(String::from).collect::<Vec<String>>();
            fields.append(&mut split_fields);
        }

        let jql = if let Some(query) = self.query {
            query
        } else {
            let mut jql_buf = String::from("resolution = unresolved");
            if let Some(project) = self.project {
                jql_buf.push_str(" AND project = ");
                jql_buf.push_str(&project);
            }
            if let Some(component) = self.component {
                jql_buf.push_str(" AND component = }");
                jql_buf.push_str(&component);
            }
            if let Some(assignee) = self.assignee {
                jql_buf.push_str(" AND assignee = ");
                jql_buf.push_str(&assignee);
            }
            if let Some(issue_type) = self.issue_type {
                jql_buf.push_str(" AND issuetype = ");
                jql_buf.push_str(&issue_type);
            }
            if let Some(watcher) = self.watcher {
                jql_buf.push_str(" AND watcher = ");
                jql_buf.push_str(&watcher);
            }
            if let Some(reporter) = self.reporter {
                jql_buf.push_str(" AND reporter = ");
                jql_buf.push_str(&reporter);
            }
            if let Some(status) = self.status {
                jql_buf.push_str(" AND status = ");
                jql_buf.push_str(&status);
            }
            if let Some(sort_order) = self.sort {
                jql_buf.push_str(" ORDER BY ");
                jql_buf.push_str(&sort_order);
            }
            jql_buf
        };

        SearchRequest {
            fields: Some(fields),
            jql: Some(jql),
            validate_query: Some(true),
            start_at: Some(0 as i64),
            max_results: Some(self.max_results as i64),
        }
    }
}

pub async fn list_issues_for_project(
    client: &Client,
    project: &str,
) -> Result<Vec<IssueListEntry>, Box<AnthologyError>> {
    tracing::info!("Listing projects for {}", client.server.host);
    let params = [("jql", format!("project={}", project))]; //&fields=\"key,name\"", project))];
    let authorization = base64::encode(format!("{}:{}", client.username, client.token));
    let endpoint = format!("{}/rest/api/2/search", client.server.host);
    let response = client
        .client
        .get(&endpoint)
        .query(&params)
        .header(AUTHORIZATION, format!("Basic {}", authorization))
        .send()
        .await?
        .error_for_status()?;
    let response_text = response.text().await?;
    let parsed_issues = serde_json::from_str::<SearchResults>(&response_text)?;
    let issues = parsed_issues
        .issues
        .ok_or("Failed to get issues")?
        .into_iter()
        .map(Into::into)
        .collect::<Vec<IssueListEntry>>();
    Ok(issues)
}

impl Into<IssueListEntry> for SearchResultsItemIssues {
    fn into(self) -> IssueListEntry {
        todo!();
        /*
        IssueListEntry {
            id: self.key.unwrap_or("Unknown".to_string()),
            title: self.name.unwrap_or("Unknown".to_string()),
            project_id: self.project.unwrap_or("Unknown".to_string()),
            description: self.description,
            url: self.self_.unwrap_or("".to_string()),
        }
        */
    }
}
