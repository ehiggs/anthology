use crate::rest::model::ListofProject::ListofProjectItemTimeZone;
use crate::rest::{Client, PaginatedResponse};
use anthology_model::{AnthologyError, Project};
use reqwest;
use reqwest::header::AUTHORIZATION;
use serde::{self, Deserialize, Serialize};
pub async fn list_projects(client: &Client) -> Result<Vec<Project>, Box<AnthologyError>> {
    // curl http://localhost:8080/rest/api/2/project
    tracing::info!("Listing projects for {}", client.server.host);
    let authorization = base64::encode(format!("{}:{}", client.username, client.token));
    let endpoint = format!("{}/rest/api/2/project/search", client.server.host);
    let response = client
        .client
        .get(&endpoint)
        .header(AUTHORIZATION, format!("Basic {}", authorization))
        .send()
        .await?
        .error_for_status()?;
    let status = response.status();
    let response_text = response.text().await?;
    tracing::debug!("list_projects returned {} - {}", status, response_text,);
    let parsed_projects =
        serde_json::from_str::<PaginatedResponse<ListofProjectItemTimeZone>>(&response_text)?;
    let projects = parsed_projects
        .values
        .into_iter()
        .map(Into::into)
        .collect::<Vec<Project>>();
    Ok(projects)
}

impl Into<Project> for ListofProjectItemTimeZone {
    fn into(self) -> Project {
        Project {
            name: self.name.unwrap_or("Unknown".to_string()),
            id: self.id.unwrap_or("Unknown".to_string()),
        }
    }
}
