pub mod model;
pub mod project;
pub mod search;
use anthology_model::{
    AnthologyError, MultiProjectApi, Project, ProjectApi, ProjectListEntry, Server, TrackerApi,
};
use async_trait::async_trait;
use serde::{self, Deserialize, Serialize};
/// JiraClient is a REST client for Jira.
/// Documentation: https://developer.atlassian.com/server/jira/platform/jira-rest-api-examples/#other-examples
#[derive(Clone, Debug)]
pub struct Client {
    pub(crate) server: Server,
    pub(crate) client: reqwest::Client,
    pub(crate) username: String,
    pub(crate) token: String,
}

impl Client {
    pub fn authenticated(server: Server, username: &str, token: &str) -> Self {
        Client {
            server,
            client: reqwest::Client::new(),
            username: username.to_string(),
            token: token.to_string(),
        }
    }
}
#[async_trait]
impl MultiProjectApi for Client {
    async fn get_project(&self, project_id: &str) -> Result<Project, Box<AnthologyError>> {
        unimplemented!();
    }
    async fn list_projects(&self) -> Result<Vec<ProjectListEntry>, Box<AnthologyError>> {
        unimplemented!();
    }
}

#[async_trait]
impl ProjectApi for Client {}
#[derive(Serialize, Deserialize)]
struct PaginatedResponse<T> {
    #[serde(rename = "self")]
    self_field: String,
    #[serde(rename = "maxResults")]
    max_results: usize,
    #[serde(rename = "startAt")]
    start_at: usize,
    total: usize,
    #[serde(rename = "isLast")]
    is_last: bool,
    values: Vec<T>,
}
