use crate::graphql::list_issues_query::IssueState;
use anthology_model::{AnthologyError, IssueListEntry};
use graphql_client::{GraphQLQuery, Response};
use reqwest::header::AUTHORIZATION;
#[derive(GraphQLQuery)]
#[graphql(
    schema_path = "graphql/gitlab.graphql",
    query_path = "graphql/gitlab_list_issues_for_project.graphql",
    response_derives = "Debug"
)]
pub struct ListIssuesQuery;

impl std::fmt::Display for IssueState {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{:?}", self)
    }
}
// TODO: fix abomination
fn extract_assignee_name(
    assignees: &Vec<Option<list_issues_query::ListIssuesQueryProjectIssuesNodesAssigneesNodes>>,
) -> String {
    if let Some(assignee) = assignees.first() {
        if let Some(ref user) = assignee {
            user.name.to_string()
        } else {
            "Unassigned".to_string()
        }
    } else {
        "Unassigned".to_string()
    }
}

// TODO: Split this into get data and parse data.
// Tests can be written on parse data; 2ez.
// Client from Anthology can run the request using client.header(Authorization, token)... without passing crap around.

pub async fn list_issues(
    client: &crate::Client,
    repository: &str,
) -> Result<Vec<IssueListEntry>, Box<AnthologyError>> {
    let variables = list_issues_query::Variables {
        project: repository.to_string(),
    };
    let request_body = ListIssuesQuery::build_query(variables);
    let request = client.client.post(&client.server.host);
    let request = match &client.token {
        Some(token) => request.header(AUTHORIZATION, format!("Bearer {}", token.clone())),
        None => request,
    };
    let res = request.json(&request_body).send().await?;

    let response_body: Response<list_issues_query::ResponseData> = res.json().await?;
    println!("Response from Server: {:?}", response_body);
    let project = response_body.data.unwrap().project.unwrap();
    let project_id = project.id;
    //let project_name = project.name;
    let raw_issue_list = project.issues.unwrap().nodes.unwrap();
    let issues: Vec<IssueListEntry> = raw_issue_list
        .into_iter()
        .map(Option::unwrap)
        .map(|issue| {
            let assignees = issue.assignees.unwrap();
            let assignee = extract_assignee_name(&assignees.nodes.unwrap());
            IssueListEntry {
                id: issue.id,
                title: issue.title,
                description: issue.description.unwrap_or("".to_string()),
                project_id: Some(project_id.clone()),
                url: issue.web_url,
            }
        })
        .collect::<_>();
    Ok(issues)
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        let result = 2 + 2;
        assert_eq!(result, 4);
    }
}
