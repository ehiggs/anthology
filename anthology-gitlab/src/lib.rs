mod graphql;

use anthology_model::{
    AnthologyError, Board, Issue, IssueListEntry, Project, ProjectListEntry, ProjectSearchProvider,
    Server, TrackerApi,
};
use async_trait::async_trait;
use crossbeam::channel;
/// Client is an identity bound to a server.
/// One can have multiple identifies on the same server. e.g. work Github and personal Github.
#[derive(Debug, Clone)]
pub struct Client {
    server: Server,
    client: reqwest::Client,
    username: Option<String>,
    token: Option<String>,
}

impl Client {
    pub fn anonymous(server: Server) -> Self {
        Self {
            server,
            client: reqwest::Client::new(),
            username: None,
            token: None,
        }
    }
    pub fn authenticated(server: Server, username: &str, token: &str) -> Self {
        Self {
            server,
            client: reqwest::Client::new(),
            username: Some(username.to_string()),
            token: Some(token.to_string()),
        }
    }
    pub async fn list_issues_for_project(
        &self,
        project_id: &str,
    ) -> Result<Vec<IssueListEntry>, Box<AnthologyError>> {
        let client = self.clone();
        let result = graphql::list_issues(&client, project_id).await?;
        Ok(result)
    }
    //pub async fn show_organization(&self, name: &str) {}
    //pub async fn list_projects_for_organization(&self, organization: &str) {}
    //pub async fn show_project(&self, project: &str) {}
    pub async fn list_projects(&self) -> Result<Vec<Project>, Box<AnthologyError>> {
        Ok(vec![])
    }
    //pub async fn show_organization(&self, name: &str) {}
    //pub async fn list_projects_for_organization(&self, organization: &str) {}
    //pub async fn show_project(&self, project: &str) {}
    pub async fn list_boards_for_project(
        &self,
        _project: &str,
    ) -> Result<Vec<Board>, Box<AnthologyError>> {
        Ok(vec![])
    }
    pub async fn get_board(&self, _id: &str) -> Result<Board, Box<AnthologyError>> {
        unimplemented!()
    }
}

#[async_trait]
impl TrackerApi for Client {
    async fn get_project(&self, project_id: &str) -> Result<Project, Box<AnthologyError>> {
        unimplemented!();
    }
    async fn list_projects(&self) -> Result<Vec<ProjectListEntry>, Box<AnthologyError>> {
        unimplemented!();
    }
}

#[async_trait]
impl ProjectSearchProvider for Client {
    async fn search_project(
        &self,
        query: &str,
    ) -> Result<Vec<ProjectListEntry>, Box<AnthologyError>> {
        Ok(vec![])
    }
}
