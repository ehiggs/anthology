table! {
    board_column_issues (board_column_id, issue_id) {
        board_column_id -> Text,
        issue_id -> Text,
    }
}

table! {
    board_columns (id) {
        id -> Text,
        name -> Text,
        board_id -> Text,
    }
}

table! {
    boards (id) {
        id -> Text,
        name -> Text,
    }
}

table! {
    issues (id) {
        id -> Text,
        title -> Text,
        description -> Text,
        author -> Nullable<Text>,
        assignee -> Nullable<Text>,
        status -> Nullable<Text>,
        project_id -> Nullable<Text>,
        url -> Nullable<Text>,
    }
}

table! {
    projects (id) {
        id -> Text,
        name -> Text,
    }
}

joinable!(board_column_issues -> board_columns (board_column_id));
joinable!(board_column_issues -> issues (issue_id));
joinable!(board_columns -> boards (board_id));

allow_tables_to_appear_in_same_query!(board_column_issues, board_columns, boards, issues, projects,);
