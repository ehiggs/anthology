use ::std::fmt;

use crate::AnthologyError;
use async_trait::async_trait;
use serde::Serialize;

#[derive(Debug, Clone)]
pub struct Server {
    pub host: String,
}

impl Server {
    pub fn new(host: &str) -> Self {
        Self {
            host: host.to_string(),
        }
    }
}

#[async_trait]
pub trait MultiProjectApi: fmt::Debug {
    /// Get a project by its id.
    async fn get_project(&self, project_id: &str) -> Result<Project, Box<AnthologyError>>;
    async fn list_projects(&self) -> Result<Vec<ProjectListEntry>, Box<AnthologyError>>;
}
/// Api to support a global search for projects based on a query string.
#[async_trait]
pub trait ProjectSearchProvider {
    async fn search_project(
        &self,
        query: &str,
    ) -> Result<Vec<ProjectListEntry>, Box<AnthologyError>>;
}
/// Api to support a global search for issues based on a query string.
#[async_trait]
pub trait IssueSearchProvider {
    async fn search_issue(&self, query: &str) -> Result<Vec<IssueListEntry>, Box<AnthologyError>>;
}

#[async_trait]
pub trait ProjectApi {
    /// List the issues relating to a project.
    async fn list_issues(&self) -> Result<Vec<IssueListEntry>, Box<AnthologyError>>;
    /// Get information for an issue.
    async fn get_issue(&self, issue_id: &str) -> Result<Vec<Issue>, Box<AnthologyError>>;
}

/// result of an issue listing.
#[derive(Debug, Serialize)]
pub struct IssueListEntry {
    pub id: String,
    pub title: String,
    pub description: String,
    pub project_id: Option<String>,
    pub url: String,
}
#[derive(Debug, Default, Serialize)]
pub struct Issue {
    pub id: String,
    pub title: String,
    pub description: String,
    pub author: String,
    pub assignee: String,
    pub status: String,
    pub project_id: Option<String>,
    pub url: String,
}

/// Organization is the grouping that a project belongs to.
#[derive(Debug, PartialEq)]
struct Organization {
    name: String,
}

#[derive(Debug, Serialize, Clone)]
pub struct Project {
    pub id: String,
    pub name: String,
}
#[derive(Debug, Serialize, Clone)]
pub struct ProjectListEntry {
    pub id: String,
    pub name: String,
}

pub struct BoardColumn {
    pub name: String,
    pub issue_ids: Vec<String>,
}

#[derive(Debug, Serialize, Clone)]
pub struct Board {
    pub id: String,
    pub name: String,
    //pub columns: Vec<String>,
}
//async fn list_projects(&self) -> Result<Vec<Project>, Box<AnthologyError>>;
//async fn get_project(&self, id: &str) -> Result<Project, Box<AnthologyError>>;

pub trait BoardApi {
    fn list_boards_for_project(&self, project_id: &str) -> Result<Vec<Board>, Box<AnthologyError>>;
    fn get_board(&self, id: &str) -> Result<Board, Box<AnthologyError>>;
}

//async fn list_issues_for_project(&self, project_id: &str) -> Result<Vec<Issue>, Box<AnthologyError>>;
//async fn get_issue(&self, id: &str) -> Result<Issue, Box<AnthologyError>>;

/// Collect all the apis into a single mega api.
pub trait TrackerApi: fmt::Debug + MultiProjectApi + ProjectApi {}
