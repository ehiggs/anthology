//#[macro_use]
//extern crate diesel;
//#[macro_use]
//extern crate diesel_migrations;
use std::error::Error;

pub type AnthologyError = dyn Error + Sync + Send;

mod models;
//mod schema;

pub use models::{
    Board, BoardApi, Issue, IssueListEntry, Project, ProjectListEntry, ProjectSearchProvider,
    Server, TrackerApi,
};

//use diesel::r2d2::ConnectionManager;
//use diesel::sqlite::SqliteConnection;

//pub type DbPool = diesel::r2d2::Pool<ConnectionManager<SqliteConnection>>;

//embed_migrations!();

/*
pub fn create_database(location: &str) -> DbPool {
    let manager = ConnectionManager::<SqliteConnection>::new(location);
    let db_pool = DbPool::builder()
        .build(manager)
        .expect("Could not build db pool");
    embedded_migrations::run(&db_pool.get().unwrap()).expect("Could not create db file.");
    db_pool
}
*/
