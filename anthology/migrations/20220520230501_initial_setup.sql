CREATE TABLE IF NOT EXISTS projects(
  id INTEGER PRIMARY KEY,
  name TEXT NOT NULL
);

CREATE TABLE IF NOT EXISTS statuses(
  id INTEGER PRIMARY KEY,
  name TEXT NOT NULL
);

CREATE TABLE IF NOT EXISTS issues(
  id INTEGER PRIMARY KEY,
  title TEXT NOT NULL,
  description TEXT NOT NULL,
  author TEXT,
  assignee TEXT,
  project_id INTEGER,
  status INTEGER,
  FOREIGN KEY (project_id) REFERENCES projects(id),
  FOREIGN KEY (status) REFERENCES statuses(id)
);

CREATE TABLE IF NOT EXISTS boards(
  id INTEGER PRIMARY KEY,
  name TEXT NOT NULL
);

CREATE TABLE IF NOT EXISTS board_columns(
  id INTEGER PRIMARY KEY,
  name TEXT NOT NULL,
  board_id INTEGER,
  FOREIGN KEY (board_id) REFERENCES boards(id)
);