use std::{error::Error, net::TcpListener};

use anthology::{configuration::load_config_from_file, create_app};
use tracing_subscriber::{prelude::__tracing_subscriber_SubscriberExt, util::SubscriberInitExt};

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    dotenv::from_filename(".env")?;

    tracing_subscriber::registry()
        .with(tracing_subscriber::EnvFilter::new(
            std::env::var("RUST_LOG").unwrap_or_else(|_| "info,tower_http=debug".into()),
        ))
        .with(tracing_subscriber::fmt::layer())
        .init();

    let config = load_config_from_file("anthology.toml").unwrap_or_else(|e| {
        println!("Could not parse config: {}", e);
        std::process::exit(1);
    });

    let app = create_app(&config).await?;
    let listener = TcpListener::bind("0.0.0.0:3000").expect("Failed to bind address");
    let local_addr = listener
        .local_addr()
        .expect("Failed to convert open socket to local address");
    tracing::info!("anthology listening on {local_addr}");
    axum::Server::from_tcp(listener)
        .expect("msFailed to bind open socket to server")
        .serve(app.into_make_service())
        .await
        .unwrap();
    Ok(())
}
