use serde::{Deserialize, Serialize};
use std::collections::BTreeMap;
use std::error::Error;
use std::fs::File;
use std::io::{BufReader, Read};

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Configuration {
    pub trackers: BTreeMap<String, TrackerConfiguration>,
    pub server: ServerConfiguration,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct TrackerConfiguration {
    // URL to the issue tracking instance
    pub url: String,
    // api is the Tracker Api Type (gitlab, jira, ...)
    pub api: TrackerApiType,
    // username for authenticating.
    pub username: Option<String>,
    // envvar, libsecret key or vault key for authenticating.
    pub token_key: Option<String>,
}
/// Configuration for the server
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct ServerConfiguration {
    //
    #[serde(default = "default_host")]
    pub host: String,
    #[serde(default = "default_port")]
    pub port: u32,
}

fn default_host() -> String {
    String::from("127.0.0.1")
}

fn default_port() -> u32 {
    3000
}

/*
 * Api tells us which Api to use.
 * Further examples could be:
 * Asana, Bugzilla, Trello, Monday, NextCloud, Todoist
 */
#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(rename_all = "lowercase")]
pub enum TrackerApiType {
    /// Anthology is the built in tracker type
    Anthology,
    Gitlab,
    // experimental on the Atlassian side. Changes frequently
    JiraGraphql,
    JiraRest,
}

pub fn load_config_from_file(path: &str) -> Result<Configuration, Box<dyn Error>> {
    let file = File::open(path)?;
    let mut reader = BufReader::new(file);
    let mut file_contents = String::new();
    reader.read_to_string(&mut file_contents)?;
    let config = toml::from_str(&file_contents)?;
    tracing::info!("Loaded config: {:?}", config);
    Ok(config)
}
