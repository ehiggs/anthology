use crate::configuration::{Configuration, TrackerApiType};
use crate::error::Result;
use anthology_model::{Project, TrackerApi};
use std::collections::BTreeMap;
use std::sync::Arc;

#[derive(Debug, Clone)]
pub struct Trackers {
    config: Configuration,
    pub clients: BTreeMap<String, Arc<dyn TrackerApi>>,
}
