pub mod configuration;
mod ctx;
mod error;
mod log;
mod model;
mod tracker;
mod web;

use std::fs::File;

use crate::error::{Error, Result};
use crate::log::log_request;
use crate::model::DbPool;
use crate::model::ModelController;
use axum::extract::Path;
use axum::{
    http::{Method, Uri},
    middleware,
    response::{IntoResponse, Json, Response},
    Router,
};
use configuration::Configuration;
use ctx::Ctx;
use serde_json::json;
use sqlx::sqlite::SqlitePoolOptions;
use tower_cookies::CookieManagerLayer;
use uuid::Uuid;

async fn create_database(connection_str: &str) -> Result<DbPool> {
    if !::std::path::Path::new(connection_str).exists() {
        let message = format!(
            "DB file \"{}\" does not exist. Creating it.",
            connection_str
        );
        tracing::info!(message);
        File::create(connection_str).expect("Could not create db file.");
    }
    let pool = SqlitePoolOptions::new()
        .max_connections(5)
        .connect(connection_str)
        .await
        .map_err(|e| {
            let message = format!("Could not connect to db: {}", e);
            tracing::error!(message);
            e
        })
        .expect("Could not connect to db.");

    sqlx::migrate!()
        .run(&pool)
        .await
        .map_err(|e| {
            let message = format!("Could not run migrations: {}", e);
            tracing::error!(message);
            e
        })
        .expect("Could not run migrations");

    Ok(pool)
}

async fn main_response_mapper(
    ctx: Option<Ctx>,
    uri: Uri,
    req_method: Method,
    res: Response,
) -> impl IntoResponse {
    tracing::info!("response mapper");
    let uuid = Uuid::new_v4();
    let service_error = res.extensions().get::<Error>();
    let client_status_error = service_error.map(|se| se.client_status_and_error());
    let error_response = client_status_error
        .as_ref()
        .map(|(status_code, client_error)| {
            let client_error_body = json!({
                "error": {
                    "type": client_error.as_ref(),
                    "req_uuid": uuid.to_string(),
                }
            });
            (*status_code, Json(client_error_body)).into_response()
        });
    let client_error = client_status_error.unzip().1;
    log_request(uuid, req_method, uri, ctx, service_error, client_error).await;
    tracing::info!("{uuid} - Error: {service_error:?}");
    error_response.unwrap_or(res)
}

pub async fn create_app(config: &Configuration) -> Result<Router> {
    let db = create_database("anthology.sqlite").await?;
    let model_controller = ModelController::new(db).await?;

    let app_apis = web::routes_projects::routes(model_controller.clone())
        .merge(web::routes_issues::routes(model_controller.clone()))
        .route_layer(middleware::from_fn(web::mw_auth::mw_require_auth));

    let app = Router::new()
        .merge(app_apis)
        .merge(web::auth::routes_login::routes())
        .layer(middleware::map_response(main_response_mapper))
        .layer(middleware::from_fn_with_state(
            model_controller.clone(),
            web::mw_auth::mw_ctx_resolver,
        ))
        .layer(CookieManagerLayer::new());
    Ok(app)
}
