use crate::error::Result;
use serde::{Deserialize, Serialize};
use sqlx::Row;
use ulid::Ulid;

use super::DbPool;

#[derive(Debug, Serialize)]
pub struct Project {
    pub id: i64,
    pub name: String,
}

impl Project {
    pub async fn load(conn: &DbPool, project_id: i64) -> Result<Project> {
        tracing::info!("Loading a project");
        let row = sqlx::query(r#"SELECT id, name FROM projects WHERE id = $1"#)
            .bind(project_id)
            .fetch_one(conn)
            .await?;
        let id = row.try_get("id").expect("Could not load id for issue");
        let name = row.try_get("name").expect("Could not load name for issue");
        let project = Project { id, name };
        Ok(project)
    }

    pub async fn store(&self, conn: &DbPool, project: &Project) -> Result<Project> {
        tracing::info!("Saving a project");
        let row = sqlx::query(r#"INSERT INTO projects(id, name) VALUES ($1, $2) RETURNING *"#)
            .bind(project.id.clone())
            .bind(project.name.clone())
            .fetch_one(conn)
            .await?;
        let id = row.try_get("id").expect("Could not load id for issue");
        let name = row.try_get("name").expect("Could not load name for issue");
        let result = Project { id: id, name: name };
        Ok(result)
    }
}

/// ProjectListEntry is a thinner representation of the Project struct used in listings.
/// Project may be extended to include all the settings.
#[derive(Debug, Serialize)]
pub struct ProjectListEntry {
    pub id: i64,
    pub name: String,
}

impl ProjectListEntry {
    pub async fn load(conn: &DbPool) -> Result<Vec<ProjectListEntry>> {
        tracing::info!("Loading projects");
        let response = sqlx::query(r#"SELECT * FROM projects"#)
            .fetch_all(conn)
            .await?;
        let projects = response
            .iter()
            .map(|row| {
                let id = row.try_get("id").expect("Could not load id");
                let name = row.try_get("name").expect("Could not load name");
                ProjectListEntry { id: id, name: name }
            })
            .collect::<Vec<_>>();
        Ok(projects)
    }
}
#[derive(Debug, Deserialize)]
pub struct CreateProject {
    pub text: String,
}
impl CreateProject {
    pub async fn store(&self, conn: &DbPool) -> Result<Project> {
        tracing::info!("Creating a project");
        let response = sqlx::query(r#"INSERT INTO projects(name) VALUES($1) RETURNING id"#)
            .bind(self.text.clone())
            .fetch_one(conn)
            .await
            .expect("oops");
        let new_id = response
            .try_get("id")
            .expect("Could not get value from RETURNING");
        Ok(Project {
            id: new_id,
            name: self.text.clone(),
        })
    }
}
