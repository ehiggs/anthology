use sqlx::{Pool, Sqlite};

use crate::error::Result;

pub mod issue;
pub mod project;

pub type DbPool = Pool<Sqlite>;

#[derive(Clone)]
pub struct ModelController {
    pub projects_store: DbPool,
}

impl ModelController {
    pub async fn new(db: DbPool) -> Result<Self> {
        Ok(Self { projects_store: db })
    }
}
