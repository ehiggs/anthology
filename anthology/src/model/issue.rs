use crate::error::Result;
use serde::{Deserialize, Serialize};
use sqlx::Row;
use ulid::Ulid;

use super::DbPool;

#[derive(Debug, Serialize)]
pub struct Issue {
    pub id: i64,
    pub author: String,
    pub title: String,
    pub description: String,
    pub status: String,
    pub project_id: i64,
}

impl Issue {
    pub async fn load(conn: &DbPool, issue_id: i64) -> Result<Issue> {
        let query = r#"
    SELECT issues.id, author, title, description, statuses.name as status, project_id 
    FROM issues 
    LEFT JOIN statuses on issues.id=statuses.id
    WHERE issues.id = $1 
    "#;
        let row = sqlx::query(query).bind(issue_id).fetch_one(conn).await?;

        let id = row.try_get("id").expect("Could not load id");
        let author: &str = row.try_get("author").expect("Could not load author");
        let title: &str = row.try_get("title").expect("Could not load title");
        let description: &str = row
            .try_get("description")
            .expect("Could not load description");
        let status: &str = row.try_get("status").expect("Could not load status");
        let project_id = row
            .try_get("project_id")
            .expect("Could not load project_id");
        Ok(Issue {
            id: id,
            author: author.to_string(),
            title: title.to_string(),
            description: description.to_string(),
            status: status.to_string(),
            project_id: project_id,
        })
    }
    pub async fn store(&self, conn: &DbPool) -> Result<Issue> {
        Ok(todo!())
    }
}

#[derive(Debug, Serialize)]
pub struct IssueListEntry {
    pub id: i64,
    pub title: String,
}

impl IssueListEntry {
    pub async fn load_list_for_project(
        conn: &DbPool,
        project_id: i64,
    ) -> Result<Vec<IssueListEntry>> {
        let rows = sqlx::query(r#"SELECT id, title FROM issues WHERE project_id = $1"#)
            .bind(project_id)
            .fetch_all(conn)
            .await?;
        let entries = rows
            .iter()
            .map(|r| {
                let id = r.try_get("id").expect("Could not load id for issue");
                let title = r.try_get("title").expect("Could not load title for issue");
                IssueListEntry {
                    id: id,
                    title: title,
                }
            })
            .collect::<Vec<_>>();
        Ok(entries)
    }
}

#[derive(Debug, Deserialize)]
pub struct CreateIssue {
    pub title: String,
    pub description: String,
    pub status: i64,
    pub project_id: i64,
}
impl CreateIssue {
    pub async fn store(&self, conn: &DbPool) -> Result<Issue> {
        let row = sqlx::query(
            r#"INSERT INTO issues(title, description, status, project_id) values($1, $2, $3, $4) RETURNING *"#,
        )
        .bind(self.title.clone())
        .bind(self.description.clone())
        .bind(1)
        .bind(self.project_id)
        .fetch_one(conn)
        .await?;

        let id = row.try_get("id").expect("Could not load id");
        let title = row.try_get("title").expect("Could not load title");
        let description = row
            .try_get("description")
            .expect("Could not load description");
        let status = row.try_get("status").expect("Could not load status");
        let project_id = row
            .try_get("project_id")
            .expect("Could not load project_id");
        Ok(Issue {
            id,
            title,
            author: "Unknown".to_string(),
            description,
            status,
            project_id,
        })
    }
}
