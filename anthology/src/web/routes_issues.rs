use axum::extract::State;
use axum::routing::post;
use axum::{extract::Path, routing::get, Json, Router};

use crate::ctx::Ctx;
use crate::error::Result;
use crate::model::issue::{CreateIssue, Issue, IssueListEntry};
use crate::model::ModelController;

async fn create_issue(
    State(mc): State<ModelController>,
    ctx: Ctx,
    Json(input): Json<CreateIssue>,
) -> Result<Json<Issue>> {
    let issue = input.store(&mc.projects_store).await?;
    Ok(Json(issue))
}

async fn list_issues(
    State(mc): State<ModelController>,
    ctx: Ctx,
    Path(project_id): Path<i64>,
) -> Result<Json<Vec<IssueListEntry>>> {
    let entries = IssueListEntry::load_list_for_project(&mc.projects_store, project_id).await?;
    Ok(Json(entries))
}
async fn get_issue_by_id(
    State(mc): State<ModelController>,
    ctx: Ctx,
    Path(issue_id): Path<i64>,
) -> Result<Json<Issue>> {
    let issue = Issue::load(&mc.projects_store, issue_id).await?;
    Ok(Json(issue))
}

pub fn routes(mc: ModelController) -> Router {
    Router::new()
        .route("/issues/:project_id", get(list_issues))
        .route("/issue", post(create_issue))
        .route("/issue/:issue_id", get(get_issue_by_id))
        .with_state(mc)
}
