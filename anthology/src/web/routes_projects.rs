use axum::{
    extract::{Path, State},
    routing::get,
    Json, Router,
};
use serde::Deserialize;

use crate::{
    ctx::Ctx,
    model::{
        project::{CreateProject, Project, ProjectListEntry},
        ModelController,
    },
};

use crate::error::Result;

async fn project_create(
    State(mc): State<ModelController>,
    ctx: Ctx,
    Json(input): Json<CreateProject>,
) -> Result<Json<Project>> {
    tracing::info!("project_create");
    let project = input.store(&mc.projects_store).await.expect("oops");
    Ok(Json(project))
}

#[derive(Debug, Deserialize, Default)]
pub struct Pagination {
    pub offset: Option<usize>,
    pub limit: Option<usize>,
}

async fn projects_list(
    State(mc): State<ModelController>,
    ctx: Ctx,
) -> Result<Json<Vec<ProjectListEntry>>> {
    tracing::info!("projects_list");
    let projects = ProjectListEntry::load(&mc.projects_store).await?;
    Ok(Json(projects))
}

async fn project_show(
    State(mc): State<ModelController>,
    ctx: Ctx,
    Path(project_id): Path<i64>,
) -> Result<Json<Project>> {
    tracing::info!("projects_show");
    let project = Project::load(&mc.projects_store, project_id).await?;
    Ok(Json(project))
}

pub fn routes(mc: ModelController) -> Router {
    Router::new()
        .route("/project", get(projects_list).post(project_create))
        .route("/project/:project_id", get(project_show))
        .with_state(mc)
}
