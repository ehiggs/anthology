use axum::{routing::post, Json, Router};
use serde::Deserialize;
use serde_json::{json, Value};
use time::Duration;
use tower_cookies::{cookie::CookieBuilder, Cookies};

use crate::{web::AUTH_TOKEN, Error, Result};

pub fn routes() -> Router {
    Router::new().route("/auth/simple/login", post(auth_web_login))
}

async fn auth_web_login(cookies: Cookies, payload: Json<LoginPayload>) -> Result<Json<Value>> {
    tracing::info!("/auth/simple/login");
    if payload.username != "demo1" || payload.password != "welcome" {
        return Err(Error::LoginFail);
    }
    let cookie = CookieBuilder::new(AUTH_TOKEN, "user-1.exp.sign")
        .path("/")
        .secure(true)
        .max_age(Duration::days(1))
        .finish();
    cookies.add(cookie);
    let body = Json(json!({ "result": { "success": true}}));
    Ok(body)
}

#[derive(Debug, Deserialize)]
struct LoginPayload {
    username: String,
    password: String,
}
