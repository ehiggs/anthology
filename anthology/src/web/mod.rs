pub mod auth;
pub mod mw_auth;
pub mod routes_issues;
pub mod routes_projects;

pub const AUTH_TOKEN: &str = "auth-token";
