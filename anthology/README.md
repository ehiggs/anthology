# Anthology
The top level anthology project collects the individual trackers together behind
a single interface. This interface is currently an enum that dispatches to the
underlying implementation. 

# Current implementations
* Jira
* Gitlab

# Future implementations
* Github
* Trello
* Todoist

# Also possible

* Asana
* Azure Devops
* Basecamp
* Bitbucket Issues
* Bugzilla
* BuildUp
* Fogbugz
* Mailing Lists - Used in many big projects. Supporting this would be really interesting.
* Monday.com
* Redmine
* Team Foundation Server
* Trac
* YouTrack
* [nextcloud/deck - FOSS Kanban](https://github.com/nextcloud/deck)

Please submit more PRs for possible integrations.

# Ideas for future improvements
An i
A superior version might setup actors to manage the
projects and send the information back to the main anthology actor in messages.