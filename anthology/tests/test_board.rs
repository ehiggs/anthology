use assert_cmd::prelude::*;
use std::{error::Error, process::Command};

#[test]
fn test_board_list() -> Result<(), Box<dyn Error>> {
    let mut cmd = Command::cargo_bin("anthology")?;
    cmd.arg("board").arg("list").assert().failure().stderr("");
    Ok(())
}

#[test]
fn test_board_show() -> Result<(), Box<dyn Error>> {
    let mut cmd = Command::cargo_bin("anthology")?;
    cmd.arg("board")
        .arg("show")
        .arg("anthology-board")
        .assert()
        .failure()
        .stderr("");
    Ok(())
}
