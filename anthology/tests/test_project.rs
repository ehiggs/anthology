use assert_cmd::prelude::*;
use std::{error::Error, process::Command};

#[test]
fn test_project_list() -> Result<(), Box<dyn Error>> {
    let mut cmd = Command::cargo_bin("anthology")?;
    cmd.arg("project").arg("list").assert().failure().stderr("");
    Ok(())
}

#[test]
fn test_project_show() -> Result<(), Box<dyn Error>> {
    let mut cmd = Command::cargo_bin("anthology")?;
    cmd.arg("project")
        .arg("show")
        .arg("SERVER")
        .assert()
        .failure()
        .stderr("");
    Ok(())
}
