use assert_cmd::prelude::*;
//use predicates::prelude::*;
use std::{error::Error, process::Command};

#[test]
fn test_issue_list() -> Result<(), Box<dyn Error>> {
    let mut cmd = Command::cargo_bin("anthology")?;
    cmd.arg("issue").arg("list").assert().failure().stderr("");
    Ok(())
}

#[test]
fn test_issue_show() -> Result<(), Box<dyn Error>> {
    let mut cmd = Command::cargo_bin("anthology")?;
    cmd.arg("issue")
        .arg("show")
        .arg("HDFS-1023")
        .assert()
        .failure()
        .stderr("");
    Ok(())
}
